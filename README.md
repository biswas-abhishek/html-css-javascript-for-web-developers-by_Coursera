# HTML, CSS and Javascript for Web Developers (by Johns Hopkins University)

This repository contains my solutions to the assignments of the Coursera course "HTML, CSS, and Javascript for Web Developers" by Johns Hopkins University.
Assignments
## Module 2

   ### Assignment description
   [Solution](https://abhishek_biswas.gitlab.io/html-css-javascript-for-web-developers-by_Coursera/Assignment%202/index.html)

## Module 3

   ### Assignment description
   [Solution](https://abhishek_biswas.gitlab.io/html-css-javascript-for-web-developers-by_Coursera/Assignment%203/index.html)

## Module 4
   ### Assignment description
   [Solution](https://abhishek_biswas.gitlab.io/html-css-javascript-for-web-developers-by_Coursera/Assignment%204/index.html)

## Module 5

   ### Assignment description
   ## Solution(Comming Soon)
`