(function(window) {
    var byeSpeaker = new Object();
    var speakWord = "Good Bye";
    byeSpeaker.speak = function speak(name) {
        document.write("<br>"+speakWord + " " + name +"<br>");
    };
    window.byeSpeaker = byeSpeaker;
})(window);