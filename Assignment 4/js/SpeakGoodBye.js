(function(window) {
    var helloSpeaker = new Object();
    var speakWord = "Hello";
    helloSpeaker.speak = function(name) {
        document.write("<br>"+speakWord + " " + name +"<br>");
    };
    window.helloSpeaker = helloSpeaker;
})(window);